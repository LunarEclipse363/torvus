# Torvus

Torvus is a Discord bot designed to act as a bridge between veloren servers and discord servers

## `.env.example`

You should rename this file to `.env` and fill it out with the appropriate bot configuration.

## Running

Execute `cargo run` to run the bot.
